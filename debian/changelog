uim-chewing (0.1.0-6) unstable; urgency=medium

  * Bump Standards-Version to 4.1.4.
  * Bump compat to 11.
    * Remove unnecessary Build-Depends.
  * Update Maintainer to new mailing list (Closes: #899715).
  * Update Vcs-* fields to salsa.

 -- ChangZhuo Chen (陳昌倬) <czchen@debian.org>  Sun, 03 Jun 2018 21:18:16 +0800

uim-chewing (0.1.0-5) unstable; urgency=medium

  * Change uim-utils and uim-common in Depends to uim for uim migration
    (Closes: #875759).
  * Bump Standards-Version to 4.1.1.
    * Update d/copyright Format to copyright-format 1.0.
  * Migrate from googlecode to GitHub.
    * Update d/control Homepage to GitHub.
    * Update d/copyright Source to GitHub.
    * Update d/watch to watch GitHub.
  * Update Vcs-* fields to use https.
  * Change ChangZhuo Chen's email to czchen@debian.org.

 -- ChangZhuo Chen (陳昌倬) <czchen@debian.org>  Sun, 15 Oct 2017 17:48:10 +0800

uim-chewing (0.1.0-4) unstable; urgency=medium

  * Fix FTBFS on ppc64el (Closes: #744672)
  * Bump Standards-Version to 3.9.5
  * Add ChangZhuo Chen (陳昌倬) as Uploaders

 -- ChangZhuo Chen (陳昌倬) <czchen@gmail.com>  Mon, 14 Apr 2014 18:42:29 +0800

uim-chewing (0.1.0-3) unstable; urgency=medium

  * Team upload.
  * Urgency set medium to fix RC bug.
  * debian/uim-chewing.postinst,
    debian/uim-chewing.prerm: Change uim registration directory from
    /etc/uim to /var/lib/uim (Closes: #688233).

 -- Kan-Ru Chen (陳侃如) <koster@debian.org>  Mon, 15 Oct 2012 21:51:10 +0800

uim-chewing (0.1.0-2) unstable; urgency=low

  * Team upload.
  * Rebuild against multi-arch libuim.
  * debian/control:
    - Update Vcs-* to use new anonscm url.
    - Bump Standards-Version to 3.9.3, no changes needed.
    - Set Multi-Arch to same.
    - Build-Depends on dh-exec.
  * debian/compat:
    - Set to 9 for dh-exec compatibility.
  * debian/rules:
    - Enable hardening.

 -- Kan-Ru Chen <koster@debian.org>  Mon, 04 Jun 2012 00:27:14 +0800

uim-chewing (0.1.0-1+build1) unstable; urgency=low

  * Team upload, rebuild with Multi-Arch enabled libchewing.

 -- Aron Xu <aron@debian.org>  Tue, 03 Apr 2012 07:06:38 +0000

uim-chewing (0.1.0-1) unstable; urgency=low

  * New upstream release
  * Lower libuim-dev version requirement

 -- Kan-Ru Chen <koster@debian.org>  Mon, 12 Sep 2011 23:44:17 +0800

uim-chewing (0.0.4.2+20110825svn-1) unstable; urgency=low

  * Initial release (Closes: #639212)

 -- Kan-Ru Chen <koster@debian.org>  Thu, 25 Aug 2011 20:02:11 +0800
